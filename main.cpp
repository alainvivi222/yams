#include <iostream>
#include <random>
#include <ctime>
#include <chrono>
#include <thread>
#include <fstream>
#include<string>
#include<sstream>
#include "fonctions.h"

using namespace std;
using std::stoi;


int val_de;
int de[5];
int rep = 0;
int cpteur = 0;
int ordre;
int score[16];
int parties = 0;
int j=0;
int ligne = 0;
int cpteur_de = 0;
bool verif = false;
bool verif2 = false;
int k = 0;
int best_score_joueur[3][3];
int numero_joueur = 0;
int encore = 0;
bool nenuphar = false;
int utilisation_nenuphar[6];
int cpteur_nenuphar = 0;
bool barre = false;
int nbre_barre = 0;
bool joker = false;
int best_score_col2 = 0;
string chaine;
string tab_best_scores[10];
string prenom[3];

void ecriture(string b){
     string const nomFichier("C:/Users/VILLEFRANQUE/Dropbox/Bachelor/cours/c++/yams_version_finale/best_scores_ever.txt");
     //string const nomFichier("C:/Users/VILLEFRANQUE/Documents/best_scores_ever.txt");
     ofstream monFlux(nomFichier.c_str());
     if(monFlux)
     {
                monFlux << chaine << endl;
     }
     else
        cout << "Impossible d'ouvrir le fichier." << endl;
}

void lecture(){
     int i = 0;

     ifstream monFlux("C:/Users/VILLEFRANQUE/Dropbox/Bachelor/cours/c++/yams_version_finale/best_scores_ever.txt");
     //ifstream monFlux("C:/Users/VILLEFRANQUE/Documents/best_scores_ever.txt");
     if(monFlux)
     {

                monFlux.clear();
                //initialiser le curseur de z�ro
                monFlux.seekg(0, ios::beg);
                string mot;

                do{
                    tab_best_scores[i] = mot;
                    i++;
                }while(monFlux >> mot);
     }
     else
         cout<<"impossible d'ouvrir le fichier" <<endl;
}

int main()
{
    prenom[0] = "Alain";
    prenom[1] = "Anne";
    prenom[2] = "Invite";

    cout << "Bonjour, bienvenue dans ce jeu de Yams !" << endl << endl ;

    do{
        do{
            cout << "Si vous etes Alain, tapez 0; si vous etes Anne, tapez 1; si vous etes quelqu'un d'autre, tapez 2 :  ";
            cin >> rep;
        }while(rep != 0 && rep != 1 && rep !=2);

        numero_joueur = rep;
        cout << endl << endl << "Bienvenue " << prenom[numero_joueur] << " !";

        lecture();
        cout << endl << endl << "Voici les meilleurs scores de tous les temps avant cette partie :" <<endl;
        cout << "Alain : Categorie 0 = " << tab_best_scores[1] << " Categorie 1 = " << tab_best_scores[2] << " Categorie 2 = " << tab_best_scores[3] << endl;
        cout << "Anne : Categorie 0 = " << tab_best_scores[4] << " Categorie 1 = " << tab_best_scores[5] << " Categorie 2 = " << tab_best_scores[6] << endl;
        cout << "Invite : Categorie 0 = " << tab_best_scores[7] << " Categorie 1 = " << tab_best_scores[8] << " Categorie 2 = " << tab_best_scores[9] << endl;
        do{
            cout << endl << "Si vous voulez faire une partie normale, tapez 0; si vous voulez l'option nenuphar, tapez 1; si vous voulez l'option barre, tapez 2 : ";
            cin >> rep;
        }while(rep != 0 && rep != 1 && rep != 2);
        best_score_col2 = rep;
        if(rep==0){
            nenuphar = false;
            barre = false;
        }
        else if(rep == 1){
            barre = false;
            nenuphar = true;
            for(int i = 0;i<6;i++){
                utilisation_nenuphar[i]=0;
            }
        }
        else if(rep == 2){
            barre = true;
            nenuphar = false;
        }

        for(int i=0;i<16;i++){
            score[i] = -100;
        }

        do{
            cpteur = 0;
            parties++;
            std::srand(std::time(nullptr));

            for(int i=0; i<5; i++){
                tirage_de();
                de[i] = val_de + 1;
                cout << " de " << i + 1 << " : " << de[i] << endl ;
            }

            do{
                rep = 0;
                do{
                    cout << endl << "Si vous voulez conserver vos des, tapez 0; si vous voulez en rejouer, tapez le nombre de des a rejouer : ";
                    cin >> rep;
                }while(rep<0 || rep >5);

                if(rep != 0 && rep != 5){

                    for(int i = 0; i<rep;i++){
                        do{
                            cout << endl << "Indiquez le numero du de que vous voulez retirer : ";
                            cin >> ordre;
                        }while(ordre<1 || ordre>5);

                        tirage_de();
                        de[ordre-1] = val_de + 1;
                    }
                }
                else if(rep == 5){

                    for(int i = 0; i<5;i++){
                        tirage_de();
                        de[i] = val_de + 1;
                    }
                }
                if(rep != 0){

                    cpteur++;
                    cout << endl << "voici votre nouveau tirage : " << endl;
                    for(int i=0; i<5; i++){
                        cout << " de " << i + 1 << " : " << de[i] << endl;
                    }
                }
            }while(rep != 0 && cpteur < 2);

            affichage_tableau();
            if(nenuphar == true){
                cpteur_nenuphar = 0;
                for(int i= 0;i<6;i++){
                    if(utilisation_nenuphar[i] == 1){
                        cpteur_nenuphar++;
                    }
                }
                if(cpteur_nenuphar == 0){
                    cout << endl << "A ce stade, vous n'avez pas encore utilise de nenuphar." << endl;
                }
                else if(cpteur_nenuphar == 6){
                    cout << endl << "Vous avez deja utilise tous vos nenuphars" << endl;
                }
                else if(cpteur_nenuphar != 0 && cpteur_nenuphar != 6){
                    cout << endl << "Vous avez deja utilise le(s) nenuphar(s) suivant(s) :";
                    for(int i=0; i<6; i++){
                        if(utilisation_nenuphar[i] == 1){
                            cout << " " << i+1;
                        }
                    }
                }
                if(cpteur_nenuphar != 6)
                {
                    do{
                            cout << endl << endl << "Si vous ne souhaitez pas utiliser de nenuphar, tapez 0; sinon le nenuphar que vous vouez utiliser : ";
                            cin >> rep;

                    }while((rep<0 || rep>6) ||((rep>0) &&(utilisation_nenuphar[rep-1] == 1)));


                    if(rep != 0){
                        do{
                            cout << endl << endl << "Indiquez le numero de de que vous voulez changer par nenuphar : ";
                            cin >> ordre;
                        }while(ordre<1 || ordre >5);
                        de[ordre-1]=rep;
                        utilisation_nenuphar[rep-1] = 1;

                        cout << endl << "voici votre nouveau tirage : " << endl;
                        for(int i=0; i<5; i++){
                            cout << " de " << i + 1 << " : " << de[i] << endl;
                        }
                    }
                }
            }
            if(barre == false){

                do {
                    cout << endl << "Indiquez le numero de ligne que vous voulez completer : ";
                    cin >> ligne;
                }while((ligne<1) || (ligne>14) || (score[ligne-1] != -100));

                score[ligne-1] = compter(ligne);
                affichage_tableau();
            }
            else if(barre == true){
                cout << endl <<"Votre nombre de barres actuel : " << nbre_barre;
                if(nbre_barre != 0){
                    do{
                            cout << endl << "Si vous voulez utiliser une barre, tapez 1; sinon, tapez 0 : ";
                            cin >> rep;
                    }while(rep != 0 && rep != 1);
                    if(rep == 1){
                        joker = true;
                        nbre_barre--;
                        parties--;
                        cout << endl << " Votre nombre de barres actualise : " << nbre_barre << endl << endl;
                    }
                    else if(rep == 0){
                        joker = false;
                    }
                }
                if((joker == false) &&
                    ((score[0] == -100 || score[1] == -100 || score[2] == -100 || score[3] == -100 || score[4] == -100 || score[5] == -100 ) &&
                    (score[7] == -100 || score[8] == -100 || score[9] == -100 || score[10] == -100 || score[11] == -100 || score[12] == -100
                      || score[13] == -100))){
                    do{
                        cout << endl << "Si vous voulez faire une double marque avec obtention d'une barre, tapez 1; sinon, tapez 0 : ";
                        cin >> rep;
                    }while((rep != 0) && (rep != 1));

                    if(rep == 0){
                        do {
                            cout << endl << "Indiquez le numero de ligne que vous voulez completer : ";
                            cin >> ligne;
                        }while((ligne<1) || (ligne>14) || (score[ligne-1] != -100));

                        score[ligne-1] = compter(ligne);
                        affichage_tableau();
                    }
                    else if(rep == 1){
                        cout << endl << "Vous allez donc completer 2 lignes du tableau et obtenir une barre supplementaire ! " <<endl;

                        do {
                            cout << endl << "Indiquez le numero de ligne de 1 a 6 que vous voulez completer : ";
                            cin >> ligne;
                        }while((ligne<1) || (ligne>6) || (score[ligne-1] != -100));

                        score[ligne-1] = compter(ligne);
                        affichage_tableau();

                        do {
                            cout << endl << "Indiquez le numero de ligne de figure que vous voulez completer : ";
                            cin >> ligne;
                        }while((ligne<8) || (ligne>13) || (score[ligne-1] != -100));

                        score[ligne-1] = compter(ligne);
                        affichage_tableau();

                        nbre_barre++;
                        parties++;

                        cout << endl << endl << "Voici votre nombre de barres actualise : " << nbre_barre << endl << endl;

                    }
                }
                else if((joker == false) &&
                    ((score[0] != -100 && score[1] != -100 && score[2] != -100 && score[3] != -100 && score[4] != -100 && score[5] != -100 ) ||
                    (score[7] != -100 && score[8] != -100 && score[9] != -100 && score[10] != -100 && score[11] != -100 && score[12] != -100
                      && score[13] != -100))) {

                    do {
                        cout << endl << "Indiquez le numero de ligne que vous voulez completer : ";
                        cin >> ligne;
                    }while((ligne<1) || (ligne>14) || (score[ligne-1] != -100));

                    score[ligne-1] = compter(ligne);
                    affichage_tableau();
                }

            }

        joker = false;

        }while(parties<13);
        cpteur_de = 0;
        for(int i=0;i<6;i++){
            cpteur_de = cpteur_de + score[i];
        }
        if(cpteur_de < 0){
            score[6] = cpteur_de;
        }
        else if(cpteur_de > -1){
            score[6] = cpteur_de + 35;
        }
        cpteur_de = 0;
        for(int i = 7;i < 14;i++){
            cpteur_de = cpteur_de + score[i];
        }
        score[14] = cpteur_de;
        score[15] = score[14] + score[6];
        affichage_tableau();

        // commentaires sur le score

       lecture();

        cout << endl << endl << "pour rappel, voici les meilleurs scores de tous les temps avant cette partie :" <<endl;
        cout << "Alain : Categorie 0 = " << tab_best_scores[1] << " Categorie 1 = " << tab_best_scores[2] << " Categorie 2 = " << tab_best_scores[3] << endl;
        cout << "Anne : Categorie 0 = " << tab_best_scores[4] << " Categorie 1 = " << tab_best_scores[5] << " Categorie 2 = " << tab_best_scores[6] << endl;
        cout << "Invite : Categorie 0 = " << tab_best_scores[7] << " Categorie 1 = " << tab_best_scores[8] << " Categorie 2 = " << tab_best_scores[9] << endl;
        int score1 = stoi(tab_best_scores[(numero_joueur * 3)+ best_score_col2+1]);

        if(score[15]> score1){
                cout << endl << "Bravo " << prenom[numero_joueur] << " ! Vous etablissez un nouveau record personnel de tous les temps dans la categorie " << best_score_col2<<" avec "<< score[15] <<" points !"<< endl;
                string score2(std::to_string(score[15]));
                tab_best_scores[(numero_joueur * 3)+ best_score_col2+1] = score2;
                chaine = tab_best_scores[1]+" "+tab_best_scores[2]+" "+tab_best_scores[3]+" "+tab_best_scores[4]+" "+tab_best_scores[5]+" "+tab_best_scores[6]+" "+tab_best_scores[7]+" "+tab_best_scores[8]+" "+tab_best_scores[9];
                ecriture(chaine);
                cout << endl << endl << "Voici les meilleurs scores de tous les temps apres cette partie :" <<endl;
                cout << "Alain : Categorie 0 = " << tab_best_scores[1] << " Categorie 1 = " << tab_best_scores[2] << " Categorie 2 = " << tab_best_scores[3] << endl;
                cout << "Anne : Categorie 0 = " << tab_best_scores[4] << " Categorie 1 = " << tab_best_scores[5] << " Categorie 2 = " << tab_best_scores[6] << endl;
                cout << "Invite : Categorie 0 = " << tab_best_scores[7] << " Categorie 1 = " << tab_best_scores[8] << " Categorie 2 = " << tab_best_scores[9] << endl;
        }
        else if(score[15]<=score1){
                cout << endl << prenom[numero_joueur] << ", vous n'ameliorez pas votre record personnel de tous les temps dans la categorie " << best_score_col2 << " avec "<< score1<< " points !"<< endl;
        }

        do{
                cout << endl << "Si vous ou une autre personne souhaitez continuer a jouer, tapez 1; sinon, tapez 0 : ";
                cin >> encore;
        }while(encore != 0 && encore != 1);


    verif=false;
    cpteur = 0;
    parties = 0;
    nbre_barre =0;
    nenuphar = false;
    barre = false;
    joker = false;
    system("CLS");

    }while(encore == 1);

    cout << endl << endl << "C'est la fin du jeu !" << endl << endl;

}

int tirage_de(){
    /*uniform_int_distribution<>distr(0, 5);
    coul = distr(eng);*/
    int MAXC = 6;
    val_de=rand() % MAXC;
    return(val_de);
}

void affichage_tableau(){
    cout << endl;
    for(int i=0;i<6;i++){
        cout << "|" << " " << i+1;
        cout << "|" << " " << i+1 << " |";
        if(score[i] == -100){
            cout << " - |" <<endl;
        }
        else if((score[i] != -100) && (score[i]<-9)){
            cout << score[i] << "|" << endl;
        }
        else if((score[i] != -100) && (score[i]<0)){
            cout << " " << score[i] << "|" << endl;
        }
        else if((score[i] < 10) && (score[i] > -1)){
            cout << "  " << score[i] << "|" << endl;
        }
        else if(score[i] > 9){
            cout << " " << score[i] << "|" << endl;
        }
    }
    j=6;
    cout << "|  " << "|" << "ST1" << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if((score[j] < -9) && (score[j] != -100)){
        cout << score[j] << "|" << endl;
    }
    else if((score[j] < 0) && (score[j] > -10) && (score[j] != -100)){
        cout << " " << score[j] << "|" << endl;
    }
    else if((score[j] >-1) && (score[j] <10)){
        cout << "  " << score[j] << "|" << endl;
    }
    else if((score[j] >9) && (score[j] <100)){
        cout << " " << score[j] << "|" << endl;
    }
    else if(score[j] >99){
        cout << score[j] << "|" << endl;
    }
    j=7;
    cout << "|" << " " << j + 1;
    cout << "|" << "BRE" << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if((score[j] != -100) && (score[j] <10)){
        cout << "  " << score[j] << "|" << endl;
    }
    else if(score[j] >9){
        cout << " " << score[j] << "|" << endl;
    }
    j=8;
    cout << "|" << " " << j + 1;
    cout << "|" << "CAR" << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if((score[j] != -100) && (score[j] <10)){
        cout << "  " << score[j] << "|" << endl;
    }
    else if(score[j] >9){
        cout << " " << score[j] << "|" << endl;
    }
    j=9;
    cout << "|" << j + 1;
    cout << "|" << "FUL" << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if(score[j] == 0){
        cout << "  " << score[j] << "|" << endl;
    }
    else if(score[j] > 0){
        cout << " " << score[j] << "|" << endl;
    }
    j=10;
    cout << "|" << j + 1;
    cout << "|" << "SUI" << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if((score[j] != -100) && (score[j] != 0)){
        cout << " " << score[j] << "|" << endl;
    }
    else if(score[j] == 0){
        cout << "  " << score[j] << "|" << endl;
    }
    j=11;
    cout << "|" << j + 1;
    cout << "|" << " -8" << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if(score[j] == 0){
        cout << "  " << score[j] << "|" << endl;
    }
    else if(score[j] > 0){
        cout << " " << score[j] << "|" << endl;
    }
    j=12;
    cout << "|" << j + 1;
    cout << "|" << "YAM" << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if(score[j] == 0){
        cout << "  " << score[j] << "|" << endl;
    }
    else if(score[j] > 0){
        cout << " " << score[j] << "|" << endl;
    }
    j=13;
    cout << "|" << j + 1;
    cout << "|" << " + " << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if((score[j] != -100) && (score[j] <10)){
        cout << "  " << score[j] << "|" << endl;
    }
    else if(score[j] >9){
        cout << " " << score[j] << "|" << endl;
    }
    j=14;
    cout << "|  " << "|" << "ST2" << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if((score[j] != -100) && (score[j] <10)){
        cout << "  " << score[j] << "|" << endl;
    }
    else if((score[j] >9) && (score[j] <100)){
        cout << " " << score[j] << "|" << endl;
    }
    else if(score[j] >99){
        cout << score[j] << "|" << endl;
    }
    j=15;
    cout << "|  " << "|" << "TOT" << "|";
    if(score[j] == -100){
    cout << " - |" <<endl;
    }
    else if((score[j] != -100) && (score[j] <10)){
        cout << "  " << score[j] << "|" << endl;
    }
    else if((score[j] >9) && (score[j] <100)){
        cout << " " << score[j] << "|" << endl;
    }
    else if(score[j] >99){
        cout << score[j] << "|" << endl;
    }
}

int compter(int a){

    cpteur_de = 0;
    verif = false;
    verif2 = false;

    if(a == 1){
        for(int i=0;i<5;i++){
            if(de[i]==1){
                cpteur_de++;
            }
        }
        return(cpteur_de);
    }
    else if(a>1 && a<7){
        for(int i=0;i<5;i++){
            if(de[i]==a){
                cpteur_de++;
            }
        }
        cpteur_de = (cpteur_de - 3) * a;
        return(cpteur_de);
    }
    else if(a==8){
        int i=0;
        do{
            int j=i+1;
            do{
                if(de[i]==de[j]){
                    cpteur_de++;
                    if(cpteur_de == 2){
                        verif =true;
                    }
                }
                j++;
            }while((verif == false) && (j<5));
        cpteur_de = 0;
        i++;
        }while((verif == false) && (i<3));

        if(verif == false){
            cpteur_de = 0;
        }
        else if(verif == true){
            cpteur_de=0;
            for(int i=0;i<5;i++){
                cpteur_de = cpteur_de + de[i];
            }
        }
        return(cpteur_de);
    }
    else if(a==9){
        int i = 0;
        int j = 0;
        do{
            j=i+1;
            do{
                if(de[i] == de[j]){
                    cpteur_de++;
                    if(cpteur_de == 3){
                        verif = true;
                    }
                }
                j++;
            }while((verif == false) && (j<5));
        cpteur_de = 0;
        i++;
        }while((verif == false) && (i<2));

        if(verif == false){
            cpteur_de = 0;
        }
        else if(verif == true){
            cpteur_de=0;
            for(int i=0;i<5;i++){
                cpteur_de = cpteur_de + de[i];
            }
        }
        return(cpteur_de);
    }
    else if(a==10){
        int i=0;
        do{
            int j=i+1;
            do{
                if(de[i]==de[j]){
                    cpteur_de++;
                    if(cpteur_de == 2){
                        verif = true;
                        k=j;
                    }
                }
                j++;
            }while((verif == false) && (j<5));
        cpteur_de = 0;
        i++;
        }while((verif == false) && (i<3));

        if(verif == true){
            cpteur_de=0;
            int i=0;
                do{
                    int j=i+1;
                    do{
                        if((de[i]==de[j]) && (de[i] != de[k])){
                            cpteur_de++;
                            if(cpteur_de == 1){
                            verif2 =true;
                            }
                        }
                    j++;
                    }while((verif2 == false) && (j<5));
                    cpteur_de = 0;
                    i++;
                }while((verif2 == false) && (i<4));
        }
        else if(verif == false){
            cpteur_de = 0;
        }

        if(verif2 == false){
            cpteur_de = 0;
        }
        else if(verif2 == true){
            cpteur_de = 25;
        }
        return(cpteur_de);
    }
    else if(a==11){
        int i = 0;
        int j;
        do{
                j=i+1;
                do{
                    if(de[i]==de[j]){
                        verif = true;
                    }
                    j++;
                }while(verif==false && j<5);
                i++;
        }while(verif==false && i<4);

        if(verif == true){
            cpteur_de = 0;
        }
        else if(verif == false){
            for(int i=0;i<5;i++){
                cpteur_de = cpteur_de + de[i];
            }
            if(cpteur_de != 15 && cpteur_de != 20){
            cpteur_de = 0;
            }
            else if(cpteur_de == 15){
            cpteur_de = 30;
            }
            else if(cpteur_de ==20){
                cpteur_de = 40;
            }
        }
        return(cpteur_de);
    }
    else if(a==12){
        for(int i=0;i<5;i++){
            cpteur_de = cpteur_de + de[i];
        }
        if(cpteur_de >7)
        {
            cpteur_de = 0;
        }
        else if(cpteur_de <8){
            cpteur_de = 40;
        }
        return(cpteur_de);
    }
    else if(a==13){
        int j=1;
        do{
            if(de[0]==de[j]){
                cpteur_de++;
                if(cpteur_de == 4){
                    verif =true;
                }
            }
            j++;
        }while((verif == false) && (j<5));

       if(verif == false){
            cpteur_de = 0;
        }
        else if(verif == true){
            cpteur_de = 50;
        }
        return(cpteur_de);
    }
    else if(a==14){
        for(int i=0;i<5;i++){
            cpteur_de = cpteur_de + de[i];
        }
        return(cpteur_de);
    }
}
